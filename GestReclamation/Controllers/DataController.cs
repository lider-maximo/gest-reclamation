﻿using GestReclamation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace GestReclamation.Controllers
{
    public class DataController : Controller
    {
        public JsonResult UserLogin (LoginData d)
        {
            using (GestReclamationDBEntities dc = new GestReclamationDBEntities())
            {
                var user = dc.Users.Where(a => a.Username.Equals(d.Username) && a.Password.Equals(d.Password)).FirstOrDefault();
                return new JsonResult { Data = user, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public JsonResult UserReclamations ()
        {
            List<UserReclamations> ur = new List<UserReclamations>();
            using (GestReclamationDBEntities db = new GestReclamationDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var user = db.Users.OrderBy(a => a.Username).ToList();
                foreach (var item in user)
                {
                    var rec = db.Reclamations.Where(a => a.UserId.Equals(item.UserId)).OrderBy(a => a.RecDate).ToList();
                    ur.Add(new UserReclamations
                    {
                        user = item,
                        reclamation = rec
                    });
                }
            }
            return new JsonResult { Data = ur, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public JsonResult DisplayReclamations ()
        {
            List<Reclamation> rec = new List<Reclamation>();
            using (GestReclamationDBEntities db = new GestReclamationDBEntities())
            {
                rec = db.Reclamations.OrderByDescending(a => a.RecDate).ToList();
            } return new JsonResult { Data = rec, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}