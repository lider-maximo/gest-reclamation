﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestReclamation.Models
{
    public class UserReclamations
    {
        public virtual User user { get; set; }
        public virtual List<Reclamation> reclamation { get; set; }
    }
}