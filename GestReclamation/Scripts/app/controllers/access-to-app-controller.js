﻿(function () {
    'use strict';

    angular
        .module('app')

        .controller('AccessToAppController', function ($scope, $window, $controller) {

            $controller('LoginController', { $scope: $scope });

            $scope.RedirectToURL = function () {
                var host = $window.location.host;
                var landingUrl = "/Home/DisplayReclamations";
                var Message = "Successfully login done. Welcome.";
                alert(Message);
                $window.location.href = landingUrl;
            };
        });

})();