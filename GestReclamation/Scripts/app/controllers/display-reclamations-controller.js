﻿(function () {
    'use strict';

    angular
        .module('app')

        .controller('DisplayReclamationsController', function ($scope, DisplayReclamationsService) {

            $scope.reclamations = [];

            DisplayReclamationsService.GetReclamations().then(function (d) {
                $scope.reclamations = d.data;
            });
        })

        .factory('DisplayReclamationsService', function ($http) {

            var fac = {};

            fac.GetReclamations = function () {
                return $http.get('/Data/UserReclamations');
            }

            return fac;
        });

})();