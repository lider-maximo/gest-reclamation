﻿(function () {
    'use strict';

    angular
        .module('app')

        .controller('ReclamationsController', function ($scope, ReclamationsService) {

            $scope.Reclamations = null;

            // Popup variables
            $scope.showModal = false;
            $scope.buttonClicked = "";
            $scope.toggleModal = function (btnClicked) {
                $scope.buttonClicked = btnClicked;
                $scope.showModal = !$scope.showModal;
            };

            $scope.templates =
                [{ name: 'AddTemplate.html', url: '/Templates/AddTemplate.html' },
                { name: 'template2.html', url: 'template2.html' }];
            $scope.template = $scope.templates[0];

            // Export Data to Excel file
            $scope.exportData = function () {
                var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "Report.xls");
            };

            ReclamationsService.GetReclamationsList().then(function (d) {
                $scope.Reclamations = d.data;
            }, function (error) {
                alert('Error');
            }
            );
        })

        .factory('ReclamationsService', function ($http) {

            var fac = {};

            fac.GetReclamationsList = function () {
                return $http.get('/Data/DisplayReclamations');
            }

            return fac;
        })

        .directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
                '<div class="modal-dialog">' +
                  '<div class="modal-content">' +
                    '<div class="modal-header">' +
                      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                      '<h4 class="modal-title">Add a new reclamation</h4>' +
                    '</div>' +
                    '<div class="modal-body" ng-transclude></div>' +
                  '</div>' +
                '</div>' +
              '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.$watch(attrs.visible, function (value) {
                    if (value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });

})();