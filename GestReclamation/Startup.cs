﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestReclamation.Startup))]
namespace GestReclamation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
